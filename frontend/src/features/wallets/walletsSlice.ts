import { createSlice } from "@reduxjs/toolkit";
import type {PayloadAction} from "@reduxjs/toolkit";

import {
  WALLET_CONNECTION_STATUS,
  WALLET_CONNECTION_STATUS_TYPE,
} from "./consts";

import type { RootState } from "../../app/store";

// Define a type for the slice state
interface WalletsState {
  ethereum: WALLET_CONNECTION_STATUS_TYPE;
}

// Define the initial state using that type
const initialState: WalletsState = {
  ethereum: WALLET_CONNECTION_STATUS.INITIAL,
};

export const walletsSlice = createSlice({
  name: "wallets",
  initialState,
  reducers: {
    setEthereumWalletStatus: (state, action: PayloadAction<WALLET_CONNECTION_STATUS_TYPE>) => {
      state.ethereum = action.payload;
    },
  },
});

// actions
export const { setEthereumWalletStatus } = walletsSlice.actions;

// selectors
export const selectEthereumStatus = (state: RootState) =>
  state.wallets.ethereum;
export const selectIsEthereumConnected = (state: RootState) =>
  state.wallets.ethereum === WALLET_CONNECTION_STATUS.CONNECTED;

export default walletsSlice.reducer;

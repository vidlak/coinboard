import Web3 from "web3";
import {
  WALLET_CONNECTION_STATUS,
  WALLET_CONNECTION_STATUS_TYPE,
} from "./consts";

export function connectEthereumWallet(
  setWalletStatus: (status: WALLET_CONNECTION_STATUS_TYPE) => void,
  setAccounts: (accounts: any) => void
) {
  setWalletStatus(WALLET_CONNECTION_STATUS.CONNECTED);
 
  window.addEventListener('load', async () => {
    if (typeof window.ethereum !== "undefined") {
      // Instance web3 with the provided information
      var web3 = new Web3(window.ethereum);

      // TODO: To implement any behaviour on metamask address change
      // window.ethereum.on('accountsChanged', (accounts: any) => { /* ??? */});
      try {
        // Request account access
        await web3.eth.requestAccounts();
        console.log("The website have access to ethereum wallet");
        setWalletStatus(WALLET_CONNECTION_STATUS.CONNECTED);
      } catch (e) {
        // User denied access
        console.log("User denied access to ethereum wallet");
        setWalletStatus(WALLET_CONNECTION_STATUS.REFUSED);
      }
  
      // It only returns first account. It is related to provider used.
      // source: https://ethereum.stackexchange.com/questions/41283/why-does-web3-eth-getaccounts-return-only-1-account
      const accounts = await web3.eth.getAccounts();
  
      setAccounts(accounts);
    } else {
      console.log("window.ethereum is undefined");
      setWalletStatus(WALLET_CONNECTION_STATUS.NOT_EXISTING);
    }
  });
}

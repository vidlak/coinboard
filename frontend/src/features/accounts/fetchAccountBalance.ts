import { ACCOUNT_TYPE } from "./accountsSlice";

function _typeToWeb3Type(type: ACCOUNT_TYPE) {
  switch(type) {
    default:
      return 'eth';
  }
}

export default async function fetchAccountBalance(type: ACCOUNT_TYPE, address: string) {
  const web3Type = _typeToWeb3Type(type);
  return window.web3[web3Type].getBalance(address);
}

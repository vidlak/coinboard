import { useEffect, useState } from "react";
import { createTheme, Grid, ThemeProvider } from "@mui/material";
import { Container } from "@mui/system";

import themeOptions from "../theme";
import TopBar from "./TopBar";

import redstone from "redstone-api";
import PricesContext, { PricesContextI } from "../contexts/PricesContext";
import AccountsPage from "./AccountsPage";
import HistoryPage from "./HistoryPage";
import InvestmentsPage from "./InvestmentsPage";
import {
  setEthereumWalletStatus,
  selectIsEthereumConnected,
} from "../features/wallets/walletsSlice";
import CircularProgressCenter from "./CircularProgressCenter";
import { connectEthereumWallet } from "../features/wallets/connectEthereumWallet";
import { setAccounts } from "../features/accounts/accountsSlice";
import { useAppDispatch, useAppSelector } from "../app/hooks";

const theme = createTheme(themeOptions);

function CoinBoardApp() {
  const [prices, setPrices] = useState<PricesContextI>({
    ETH: undefined,
    DOT: undefined,
  });

  const isEthereumConnected = useAppSelector(selectIsEthereumConnected);

  const dispatch = useAppDispatch();

  const { connected } = { connected: false };

  // TODO: Move prices to store as feature
  useEffect(() => {
    Object.keys(prices).forEach(function (key: string, _index) {
      if (!prices[key]) {
        redstone.getPrice(key).then((response) => {
          setPrices((prices) => ({ ...prices, [key]: response.value }));
        });
      }
    });
  }, []);

  useEffect(() => {
    connectEthereumWallet(
      (status) => dispatch(setEthereumWalletStatus(status)),
      (accounts) => dispatch(setAccounts({type: 'ethereum', accounts}))
    );
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <PricesContext.Provider value={prices}>
        <Container fixed>
          <TopBar loggedIn={connected} />
          {isEthereumConnected ? (
            (window.location.pathname === "/" && <AccountsPage />) ||
            (window.location.pathname.indexOf("/investments") === 0 && (
              <InvestmentsPage />
            )) ||
            (window.location.pathname.indexOf("/history") === 0 && (
              <HistoryPage />
            ))
          ) : (
            <Grid container justifyContent="center">
              <Grid item marginTop="8em">
                <CircularProgressCenter />
                {/* <ConnectButton /> */}
              </Grid>
            </Grid>
          )}
        </Container>
      </PricesContext.Provider>
    </ThemeProvider>
  );
}

export default CoinBoardApp;

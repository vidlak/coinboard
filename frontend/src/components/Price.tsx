import { useContext } from "react";
import PricesContext from "../contexts/PricesContext";
import CircularProgress from "@mui/material/CircularProgress";
import { Typography } from "@mui/material";

type PriceProps = {
  // The price is provided in the smallest possible unit
  price: string;
  // how much to devide by to get the proper crypto currency unit.
  devide?: number;
  toFixed?: number;
  [key: string]: any;
};

function Price({ price, toFixed = 5, devide = 1000000000000000000, ...rest }: PriceProps) {
  const { ETH } = useContext(PricesContext);

  if (!price || !ETH) {
    return <CircularProgress />;
  }

  const priceETH = parseFloat(price) / devide;
  const usdPrice = priceETH * ETH;

  return (
    <Typography title={Number(priceETH).toFixed(toFixed) + " ETH"} {...rest}>
      ${Number(usdPrice).toFixed(2)}
    </Typography>
  );
}

export default Price;

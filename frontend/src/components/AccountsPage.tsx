import AddAddressForm from "./AddAddressForm";
import CoinsList from "./CoinsList";
import TotalBalanceFooter from "./TotalBalanceFooter";

const AccountsPage = () => (
  <>
    <CoinsList />
    <TotalBalanceFooter />
    <AddAddressForm />
  </>
);

export default AccountsPage;

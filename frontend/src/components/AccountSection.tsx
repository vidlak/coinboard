import { Button, Grid, Typography } from "@mui/material";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import Price from "./Price";
import CircularProgressCenter from "./CircularProgressCenter";
import { useEffect } from "react";
import { requestAccountBalance } from "../features/accounts/accountsSlice";
import { useAppDispatch } from "../app/hooks";

type AccountSectionProps = {
  address: string;
  index: number;
  balance?: string;
};

function AccountSection({ address, balance, index }: AccountSectionProps) {
  const dispatch = useAppDispatch();

  useEffect(() => {
    if(typeof balance === "undefined") {
      dispatch(requestAccountBalance({type: 'ethereum', address}))
    }
  }, []);

  if(typeof balance === "undefined") {
    return <CircularProgressCenter />
  }

  return (
    <>
      <Grid
        container
        borderBottom="1px solid #CCC"
        paddingTop="0.7em"
        paddingBottom="1.2em"
        alignItems="flex-end"
      >
        <Grid item xs={7}>
          <Grid container alignItems="flex-end" marginBottom="0.3em">
            <Grid item xs={7}>
              <Typography fontWeight="bold">
                Account {index+1}&nbsp;
                {/* TODO: Get ENS Domain */}
                (wolny.eth)
              </Typography>
            </Grid>
            <Grid item xs={5}>
              <Button
                style={{
                  lineHeight: "0.8em",
                  position: "relative",
                  top: "-0.4em",
                }}
                color="secondary"
                variant="outlined"
                size="small"
                onClick={(e) => {
                  e.preventDefault();
                  window.location.href = "/history/" + address;
                }}
              >
                history
              </Button>
              <Button
                style={{
                  lineHeight: "0.8em",
                  position: "relative",
                  top: "-0.4em",
                  marginLeft: "0.5em",
                }}
                color="secondary"
                variant="outlined"
                size="small"
                href={`https://buy.ramp.network/?userAddress=${address}&defaultAsset=ETH_ETH`}
                target="_blank"
              >
                TOP UP
              </Button>
            </Grid>
          </Grid>
          <Typography fontSize="0.8em">
            <a
              onClick={(e) => {
                e.preventDefault();
                navigator.clipboard.writeText(address);
              }}
              style={{ cursor: "pointer" }}
            >
              <ContentCopyIcon color="primary" style={{ fontSize: "1em" }} />
            </a>{" "}
            {address}
          </Typography>
        </Grid>
        <Grid item xs={2} textAlign="right">
          <Price
            price={balance.toString()}
            fontWeight={"bold"}
            fontSize="1.2em"
          />
        </Grid>
        <Grid item xs={3} textAlign="right">
          <Button
            variant="contained"
            style={{ paddingLeft: "40px", paddingRight: "40px" }}
          >
            TRANSFER
          </Button>
        </Grid>
      </Grid>
    </>
  );
}

export default AccountSection;

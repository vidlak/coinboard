import { Button, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import CircularProgressCenter from "./CircularProgressCenter";
import TransactionList from "./TransactionList";

const HistoryPage = () => {
  const [transactions, setTransactions] = useState([]);

  // TODO: implement the case when the address is empty
  const address: string = window.location.pathname.split("/").at(-1) ?? "";

  useEffect(() => {
    fetch(`http://localhost:3001/api/transactions?address=${address}`)
      .then((response) => {
        // TODO: Add error reporting
        return response.json();
      })
      .then((json) => {
        setTransactions(json);
      });
  }, []);

  return (
    <Grid container>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        <Typography variant="h4" style={{ fontWeight: "bold" }}>
          Transaction history - last 30 days
        </Typography>
        <Typography> for {address}</Typography>
        <div style={{ paddingTop: "2em", marginBottom: "2em" }}>
          {!transactions.length ? (
            <CircularProgressCenter />
          ) : (
            <TransactionList myAddress={address} transactions={transactions} />
          )}
        </div>
        <Button
          variant="outlined"
          color="secondary"
          style={{ marginBottom: "3em" }}
        >
          ← Back to your accounts
        </Button>
      </Grid>
    </Grid>
  );
};

export default HistoryPage;

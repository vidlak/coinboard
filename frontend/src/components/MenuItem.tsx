import { Link } from "@mui/material";
import { CSSProperties } from "react";

type MenuItemProps = {
  href: string;
  active?: boolean;
  children?: any;
};

function MenuItem({ href, active = false, children }: MenuItemProps) {
  const activeStyle: CSSProperties = active
    ? {
        textDecoration: "underline",
        textDecorationThickness: "3px",
        textUnderlineOffset: "12px",
        textDecorationColor: "#b08b20",
      }
    : {
        fontSize: "1.3em",
        textDecoration: "none",
      };

  const style: CSSProperties = {
    cursor: "pointer",
    paddingRight: "3em",
    fontSize: "1.3em",
    textTransform: "uppercase",
    ...activeStyle,
  };

  return (
    <Link
      href={href}
      onClick={(e) => {
        e.preventDefault();
        window.location.href = href;
      }}
      color="#000"
      style={style}
    >
      {children}
    </Link>
  );
}

export default MenuItem;

import React from "react";
import { Grid, Typography } from "@mui/material";
import AccountSection from "./AccountSection";
import Price from "./Price";
import { Account, getEthereumAccounts } from "../features/accounts/accountsSlice";
import { useAppSelector } from "../app/hooks";

type CoinSectionProps = {
  address: string;
  balance: string;
};

function CoinSection({ address, balance }: CoinSectionProps) {
  const ethereumAccounts = useAppSelector(getEthereumAccounts);

  if(!ethereumAccounts.length) {
    return null;
  }

  const accountList = ethereumAccounts.map((account: Account, index: number) => 
    <AccountSection {...account} key={account.address} index={index} />
  );

  return (
    <>
      {/* header */}
      <Grid container borderBottom="1px solid #CCC">
        <Grid item xs={9}>
          <Typography>ETHEREUM</Typography>
        </Grid>
        <Grid item xs={3} textAlign="right">
          <Price price={balance.toString()} fontSize="0.8em" />
        </Grid>
      </Grid>
      {accountList}
    </>
  );
}

export default CoinSection;

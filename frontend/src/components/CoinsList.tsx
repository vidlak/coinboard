import { Grid } from "@mui/material";
import CoinSection from "./CoinSection";
import TotalBalanceFooter from "./TotalBalanceFooter";
import CircularProgressCenter from "./CircularProgressCenter";

function CoinsList() {
  const { address } = { address: "" }; //useAccount();
  const { isLoading, balance } = { isLoading: 0, balance: "10" }; /*useBalance({
        addressOrName: address,
        chainId: "eip155:1",
        formatUnits: "wei"
    });*/

  return (
    <Grid container>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        {isLoading || !balance ? (
          <CircularProgressCenter />
        ) : (
          <CoinSection address={address} balance={balance} />
        )}
      </Grid>
      {!!balance && <TotalBalanceFooter balance={balance} />}
    </Grid>
  );
}

export default CoinsList;

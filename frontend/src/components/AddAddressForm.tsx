import { Button, Grid, MenuItem, TextField, Typography } from "@mui/material";
import { ChangeEvent, useCallback, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { ACCOUNT_TYPE, addAccount, createSelectEthereumAccountByAddress } from "../features/accounts/accountsSlice";

function AddAddressForm() {
  const [address, setAddress] = useState("");
  const [type, setType] = useState<ACCOUNT_TYPE>("ethereum");
  const [error, setError] = useState<string>("");

  const dispatch = useAppDispatch();
  const cleanForm = useCallback(() => {
    setAddress("");
  },[setAddress]);

  const onChangeAddress = useCallback((e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setAddress(e.target.value);
  }, [setAddress]);

  // TODO: restrict types to SelectChangeEvent<ACCOUNT_TYPE>
  const onChangeType = useCallback((e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setType(e.target.value as ACCOUNT_TYPE);
  },[setType]);

  const onClickAdd = useCallback(() => {
    dispatch(addAccount({type, address}))
    console.log("Add address");
    cleanForm();
  }, [type, address, cleanForm, dispatch]);

  const selectEthereumAccount = createSelectEthereumAccountByAddress(address);
  const isAddressDuplicated = !!useAppSelector(selectEthereumAccount);

  // address validation
  useEffect(() => {
    if(type === 'ethereum' && !!address) {
      if(!window.web3.utils.isAddress(address)) {
        setError("The address is invalid!");
      } else if(isAddressDuplicated) {
        setError("The address already added!")
      } else {
        setError("");
      }
    } else {
      // skip validation for other than ethereum account type
      setError("");
    }

  }, [address, type, isAddressDuplicated]);

  return (
    <Grid container mt={4}>
      <Grid xs={2}></Grid>
      <Grid xs={8}>
        <Typography variant="h6">
          Add Account:
        </Typography>
        <hr />
      </Grid>
      <Grid item xs={2}></Grid>

      <Grid item xs={2}></Grid>
      <Grid item xs={2}>
        <TextField select defaultValue={type} onChange={onChangeType} size="small">
          <MenuItem value={'ethereum'}>Ethereum</MenuItem>
        </TextField>
      </Grid>
      <Grid item xs={5}>
        <TextField fullWidth={true} value={address} onChange={onChangeAddress} helperText={error} error={!!error} size="small" />
      </Grid>
      <Grid item xs={1} textAlign={"end"}>
        <Button disabled={!!error || !address} variant="outlined" onClick={onClickAdd}>
          + Add
        </Button>
      </Grid>
    </Grid>
  );
}

export default AddAddressForm;

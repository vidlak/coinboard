import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import CoinBoardApp from "./components/CoinBoardApp";
import reportWebVitals from "./reportWebVitals";
// import Web3Context from './contexts/Web3Context';
import store from "./app/store";
import { Provider } from "react-redux";
import Web3 from "web3";

// todo - to add some default provider
window.web3 = new Web3(Web3.givenProvider || window.ethereum);

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  // In StrictMode React is calling useEffect twice in the development mode!
  <React.StrictMode>
    {/* <Web3Context.Provider value={{}}> */}
    <Provider store={store}>
      <CoinBoardApp />
    </Provider>
    {/* </Web3Context.Provider> */}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

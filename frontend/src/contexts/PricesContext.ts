import { createContext } from "react";

export interface PricesContextI {
  [key: string]: number | undefined;
}

const PricesContext = createContext<PricesContextI>({});

export default PricesContext;

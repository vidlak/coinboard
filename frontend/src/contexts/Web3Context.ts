import { createContext } from "react";

export interface Web3ContextI {
  [key: string]: any;
}

const Web3Context = createContext<Web3ContextI>({});

export default Web3Context;

import { configureStore } from "@reduxjs/toolkit";
import walletsReducer from "../features/wallets/walletsSlice";
import accountsReducer from "../features/accounts/accountsSlice";

const store = configureStore({
  reducer: {
    wallets: walletsReducer,
    accounts: accountsReducer
  },
});
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;

// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

export default store;

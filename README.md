# CoinBoard - bank-like dashboard for your crypto assets


## TODOs:
- [x] Get rid of WalletConnect
- [x] Add connecting your ethereum wallets through web3.js
- [x] Moving consts WALLETS_CONNECT_STATUS* to wallets feature directory
- [x] Moving initialize to wallets feature
- [x] Store accounts at redux
- [x] Add form for adding addresses!
- [x] Add AddAddressForm validation!
- [ ] TO DO: Get ENS
- [ ] Get arweave accounts
- [ ] Get Polkadot accounts
- [ ] Get two ethereum addresses
- [ ] Design how to add connect buttons
- [ ] Add connect buttons
~~- [ ] Make useEffect run only once!~~
  - It runs once except React.StrictMode at development build
- [ ] Making menu items underscored properly.
- [ ] Prices to feature
- [ ] Listening with your tokens
- [ ] Transaction history for your account with web3.js
- [ ] Check releasing for your assets
- [ ] Logo modification? Coinboard with "O" replaced with current big logo


# Documentation

### /frontend 
The directory contains frontend part of the app

##### How to run the frontend?

```bash
$ npm start
```

### /api
The directory contains backend part. It's an API for requesting transaction history from etherscan.io

It's listening on the port 3001

##### How to run the API?

1. Get API key from `etherscan.io`.
1. Gen projectId from `walletconnect.com`.
1. Add api keys to the file: `.env` (based on `.env.example`).
1. Add walletconnect projectId to the `frontend/src/consts/walletconnect.js`.

```bash
$ npm start
```
